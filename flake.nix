{
  description = "atsr python libraries";

  inputs = {
    settings = {
      url = "path:./nix/settings.nix";
      flake = false;
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    ply = {
      url = "gitlab:nixflakes%2Fpypi/ply/3.4";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.settings.follows = "settings";
    };
  };

  outputs = { self, flake-utils, ... }@inputs:

    flake-utils.lib.eachDefaultSystem (system: let

      settings = import inputs.settings;
      nixpkgs = inputs.nixpkgs.legacyPackages.${system};
      pythonPackages = nixpkgs.${settings.python+"Packages"};

      ply = inputs.ply.defaultPackage.${system};

      thispkg = pythonPackages.buildPythonPackage rec {
        pname = "slimit";
        version = "0.8.1";
        src = pythonPackages.fetchPypi {
          inherit pname version;
          sha256 = "f433dcef899f166b207b67d91d3f7344659cb33b8259818f084167244e17720b";
          extension = "zip";
        };
        propagatedBuildInputs = with pythonPackages; [ 
          ply
        ];
      };

      pythonPackaged = nixpkgs.${settings.python}.withPackages (p: with p; [ thispkg ]);

    in {

      defaultPackage = thispkg;
        
      devShell = nixpkgs.mkShell {
        buildInputs = [ pythonPackaged ];
        shellHook = ''
          export PYTHONPATH=${pythonPackaged}/${pythonPackaged.sitePackages}
        '';
      };

    });
}
